# Formation en Alternance

## Description :
Cette étape concerne les étudiants alternants et elle se déroulera du 21 novembre au 17 décembre 2023.

## Objectif :

- Implémnter la solution proposée dans la phase précédente (Phase 1).
- Voici l'ordre de priorité des sites : principal, 1 puis 2.

## Livrable :

- **Type :** Rapport (plus de détails dans les slides de présentation de la SAE).
- **Deadline :** 17 décembre 2023 à 23:59.
- **Une présentation finale de 15 à 20 minutes (en anglais), suivie de questions**.

## Planning :

![Planning de la SAÉ](planning.png)
