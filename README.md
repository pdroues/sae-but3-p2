# SAÉ groupe 5 part 2

## Organisation

Le dépot git est organier en plusieures sections:

- `scripts` contient toutes les configurations des equipements Cisco
- `dockers` contient le docker compose permetant de lancer tous les containers et les fichiers de configurations de ces derniers
- `rapport` contient nottre rapport (écrit en latex)
- `sujet` contient les consignes et ressources données

## Équipe

Les membres du groupe 5 sont les suivants:

- ANTUNES Mathieu
- DROUES Philemon
- TROCHEL Paul
- SAURY Clément
- THOUVEREY Paul
