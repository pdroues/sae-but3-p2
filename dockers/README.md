# Nextcloud

[docker nextcloud](https://hub.docker.com/_/nextcloud)

[docker bd](https://hub.docker.com/_/mariadb)

# GRAPHANA

default credential: admin admin

ajouter la data source http://prometheus:9090

puis ajouter les dashboard:
- node exporter: 1860
- cAdvisor: 14282

# Serveur DHCP

[Documentation](https://kea.readthedocs.io/en/latest/index.html)

[Documentation 2](https://gitlab.isc.org/isc-projects/kea-docker)

[Git](https://gitlab.isc.org/isc-projects/kea-docker)

## Pense-bête

Activer l'IPv6 sur Docker (feature experimental)

# DNS via BIND9

[docker](https://hub.docker.com/r/internetsystemsconsortium/bind9)

[documentation](https://bind9.readthedocs.io/en/v9.18.20/index.html)

# PAPERLESS

[env var](https://docs.paperless-ngx.com/configuration/)

[git](https://github.com/paperless-ngx/paperless-ngx/tree/main/docker/compose)
