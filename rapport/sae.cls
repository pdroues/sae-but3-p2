\LoadClass[11pt, a4paper, english, titlepage]{article}

%--------------------- Packages ------------------------

\usepackage[english]{babel}         % pour la langue
\usepackage[T1]{fontenc}            % pour la police
\usepackage{palatino}               % pour la police
\usepackage[margin=2cm]{geometry}   % pour les marges
\usepackage{graphicx}               % pour les images
\usepackage{float}                  % pour les images
\usepackage{subcaption}             % pour les images
\usepackage{parskip}                % pour sauter une ligne apres un paragraphe
\usepackage{hyperref}               % pour les liens
\usepackage{xcolor}                 % permet d'utiliser des commandes avec leur nom
\usepackage{listings}               % pour le code; apaprament > verbatim
\usepackage{wallpaper}

%------------------ Configuration ----------------------

% changer le nom de la liste des codes
\renewcommand{\lstlistlistingname}{List of Codes}

% couleur des liens
\hypersetup{colorlinks=true, linkcolor=black,  urlcolor=blue, citecolor=blue}

% saut de ligne paragraphe
\setlength{\parskip}{\baselineskip}

% lieu des images
\graphicspath{{images/}}

%------------------ Deffinition style/langages pour le package listings ----------------------

% Deffinition du langage docker-compose
\lstdefinelanguage{docker-compose}{
  keywords={version, services, volumes, networks, image, build, ports, environment, volumes_from, links, container_name, command, privileged, pid, devices, restart, depends_on, user, secrets, network_mode},
  keywordstyle=\color{blue}\bfseries,
  identifierstyle=\color{black},
  sensitive=false,
  comment=[l]{\#},
  commentstyle=\color{gray}\ttfamily,
  stringstyle=\color{red}\ttfamily
}

% Deffinition du style docker-compose
\lstdefinestyle{docker-compose-style}{
  language=docker-compose,
  basicstyle=\small\ttfamily,
  frame=single,
  breaklines=true,
  showstringspaces=false,
  tabsize=2,
  backgroundcolor=\color{gray!10},
  xleftmargin=0.5em,
  framexleftmargin=0.5em
}

\definecolor{cisco-keyword}{RGB}{0, 102, 204}
\definecolor{cisco-comment}{RGB}{0, 128, 0}
\definecolor{cisco-string}{RGB}{255, 0, 0}

\lstdefinelanguage{cisco}{
  sensitive=true,
  morekeywords=[1]{router, interface, ip, access-list, access-group, permit, deny, vlan},
  keywordstyle=[1]\color{cisco-keyword}\bfseries,
  morekeywords=[2]{address-family, network, neighbor, redistribute, default-information, summary-address},
  keywordstyle=[2]\color{cisco-keyword},
  morecomment=[l]{!},
  commentstyle=\color{cisco-comment}\itshape,
  morestring=[b]',
  morestring=[b]",
  stringstyle=\color{cisco-string},
}

\definecolor{mylightblue}{RGB}{173, 216, 230}

% Deffinition du style cisco
\lstdefinestyle{cisco-style}{
  language=cisco,
  basicstyle=\small\ttfamily,
  frame=single,
  breaklines=true,
  showstringspaces=false,
  tabsize=2,
  backgroundcolor=\color{mylightblue},
  xleftmargin=0.5em,
  framexleftmargin=0.5em
}

\definecolor{mybackground}{RGB}{255,253,208} % Couleur beige
\definecolor{myframe}{RGB}{0,0,0} % Couleur noire pour les contours
\definecolor{mytext}{RGB}{0,0,0} % Couleur noire pour le texte

\lstdefinestyle{mystyle}{
    basicstyle=\ttfamily\color{mytext}, % Police monospace, couleur du texte noir
    backgroundcolor=\color{mybackground}, % Couleur de l'arrière-plan beige
    frame=single, % Bordure simple
    rulecolor=\color{myframe}, % Couleur de la bordure noire
    linewidth=\linewidth,
}

%------------------ Commandes ----------------------

\newcommand{\UE}[1]{\renewcommand{\UE}{#1}}
\newcommand{\sujet}[1]{\renewcommand{\sujet}{#1}}
\newcommand{\titre}[1]{\renewcommand{\titre}{#1}}
\newcommand{\enseignant}[1]{\renewcommand{\enseignant}{#1}}
\newcommand{\eleves}[1]{\renewcommand{\eleves}{#1}}

\newcommand{\fairepagedegarde}{
\begin{titlepage}

\ThisLRCornerWallPaper{0.6}{images/SteinECL.jpg}
	\centering %Centraliser le contenu
	\includegraphics[width=0.2\textwidth]{images/logo.png}\par\vspace{1cm} %Insertion du logo
	{\scshape\LARGE IUT Informatique Paul Sabatier \par} %Nom de l'université
	\vspace{1.5cm}%Espace de 1,5cm
	{\scshape\Large \UE \\ \sujet \\ Report\par} %sous-titre
	\vspace{1cm}%Espace de 1cm
    \rule{\linewidth}{0.2 mm} \\[0.4 cm]
	{\huge\bfseries \titre \par} \
    \rule{\linewidth}{0.2 mm} \\[1.5 cm]
	\vspace{1cm}%Espace de 3cm
    
	\begin{minipage}{0.5\textwidth} %Minipage pour faire deux colonnes
		\begin{flushleft} \large %Envoyer à gauche
		\emph{\textbf{Students :}}\\ %Pour le titre au dessus des noms à gauche
        \eleves\\ %Remplacer pour chacun
		\end{flushleft}
	\end{minipage}
	~
	\begin{minipage}{0.4\textwidth}
		\begin{flushright} \large
		\emph{\textbf{Teacher :}} \\
		 \enseignant \\
		\end{flushright}
	\end{minipage}\\[4cm]
    
	\vfill
	{\large \today\par} %Affichage de la date

\end{titlepage}
}